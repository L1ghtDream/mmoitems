package net.Indyuce.mmoitems.command.mmoitems;

import io.lumine.mythic.lib.api.item.NBTItem;
import io.lumine.mythic.lib.api.util.SmartGive;
import io.lumine.mythic.lib.command.api.CommandTreeNode;
import io.lumine.mythic.lib.command.api.Parameter;
import net.Indyuce.mmoitems.ItemStats;
import net.Indyuce.mmoitems.MMOItems;
import net.Indyuce.mmoitems.MMOUtils;
import net.Indyuce.mmoitems.api.Type;
import net.Indyuce.mmoitems.api.item.mmoitem.MMOItem;
import net.Indyuce.mmoitems.api.player.PlayerData;
import net.Indyuce.mmoitems.api.util.message.Message;
import net.Indyuce.mmoitems.command.MMOItemsCommandTreeRoot;
import net.Indyuce.mmoitems.stat.data.SoulboundData;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

public class TakeCommandTreeNode extends CommandTreeNode {
    private static final Random random = new Random();

    public TakeCommandTreeNode(CommandTreeNode parent) {
        super(parent, "take");

        addParameter(MMOItemsCommandTreeRoot.TYPE);
        addParameter(MMOItemsCommandTreeRoot.ID_2);
        addParameter(Parameter.PLAYER);
        addParameter(Parameter.AMOUNT);
    }

    @Override
    public CommandResult execute(CommandSender sender, String[] args) {
        if (args.length != 5) {
            return CommandResult.THROW_USAGE;
        }

        try {
            // target
            Type type = MMOItems.plugin.getTypes().getOrThrow(args[1].toUpperCase().replace("-", "_"));
            String id = args[2].toUpperCase().replace("-", "_");
            Player target = Bukkit.getPlayer(args[3]);
            Validate.notNull(target, "Could not find player called '" + args[3] + "'.");
            AtomicReference<Integer> amount = new AtomicReference<>(Integer.parseInt(args[4]));

            for (int i = 0; i < target.getInventory().getSize(); i++) {
                ItemStack item = target.getInventory().getItem(i);
                if(item == null)
                    continue;

                NBTItem nbtItem = NBTItem.get(item);
                String currentType = nbtItem.getType();
                if(currentType==null){
                    continue;
                }
                if (currentType.equals(type.toString()) &&
                        nbtItem.getString("MMOITEMS_ITEM_ID").equals(id)) {
                    int removeAmount = Math.min(amount.get(), item.getAmount());
                    amount.updateAndGet(v -> v - removeAmount);
                    item.setAmount(item.getAmount() - removeAmount);
                    target.getInventory().setItem(i, item);
                }
            }

            return CommandResult.SUCCESS;

        } catch (IllegalArgumentException exception) {
            sender.sendMessage(MMOItems.plugin.getPrefix() + ChatColor.RED + exception.getMessage());
            return CommandResult.FAILURE;
        }
    }
}
